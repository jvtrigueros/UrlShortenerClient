﻿namespace UrlShortenerClient
{
    partial class Client
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.controlPage = new System.Windows.Forms.TabPage();
            this.linkPanel = new System.Windows.Forms.Panel();
            this.shortUrlLink = new System.Windows.Forms.LinkLabel();
            this.shortenButton = new System.Windows.Forms.Button();
            this.longUrlLink = new System.Windows.Forms.TextBox();
            this.settingsPage = new System.Windows.Forms.TabPage();
            this.statusBar = new System.Windows.Forms.StatusStrip();
            this.statusBarText = new System.Windows.Forms.ToolStripStatusLabel();
            this.tabControl1.SuspendLayout();
            this.controlPage.SuspendLayout();
            this.linkPanel.SuspendLayout();
            this.statusBar.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.controlPage);
            this.tabControl1.Controls.Add(this.settingsPage);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(405, 146);
            this.tabControl1.TabIndex = 0;
            // 
            // controlPage
            // 
            this.controlPage.Controls.Add(this.linkPanel);
            this.controlPage.Controls.Add(this.shortenButton);
            this.controlPage.Controls.Add(this.longUrlLink);
            this.controlPage.Location = new System.Drawing.Point(4, 22);
            this.controlPage.Name = "controlPage";
            this.controlPage.Padding = new System.Windows.Forms.Padding(3);
            this.controlPage.Size = new System.Drawing.Size(397, 120);
            this.controlPage.TabIndex = 0;
            this.controlPage.Text = "Control";
            this.controlPage.UseVisualStyleBackColor = true;
            // 
            // linkPanel
            // 
            this.linkPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.linkPanel.Controls.Add(this.shortUrlLink);
            this.linkPanel.Location = new System.Drawing.Point(7, 67);
            this.linkPanel.Name = "linkPanel";
            this.linkPanel.Size = new System.Drawing.Size(384, 36);
            this.linkPanel.TabIndex = 3;
            // 
            // shortUrlLink
            // 
            this.shortUrlLink.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.shortUrlLink.AutoSize = true;
            this.shortUrlLink.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.shortUrlLink.Location = new System.Drawing.Point(124, 9);
            this.shortUrlLink.Name = "shortUrlLink";
            this.shortUrlLink.Size = new System.Drawing.Size(48, 16);
            this.shortUrlLink.TabIndex = 0;
            this.shortUrlLink.TabStop = true;
            this.shortUrlLink.Text = "<null>";
            this.shortUrlLink.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.shortUrlLink.Visible = false;
            this.shortUrlLink.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.shortUrlLink_LinkClicked);
            // 
            // shortenButton
            // 
            this.shortenButton.Location = new System.Drawing.Point(286, 31);
            this.shortenButton.Name = "shortenButton";
            this.shortenButton.Size = new System.Drawing.Size(105, 23);
            this.shortenButton.TabIndex = 2;
            this.shortenButton.Text = "Shorten!";
            this.shortenButton.UseVisualStyleBackColor = true;
            this.shortenButton.Click += new System.EventHandler(this.shortenButton_Click);
            // 
            // longUrlLink
            // 
            this.longUrlLink.Location = new System.Drawing.Point(6, 33);
            this.longUrlLink.Name = "longUrlLink";
            this.longUrlLink.Size = new System.Drawing.Size(274, 20);
            this.longUrlLink.TabIndex = 1;
            this.longUrlLink.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.longUrlLink_KeyPress);
            // 
            // settingsPage
            // 
            this.settingsPage.Location = new System.Drawing.Point(4, 22);
            this.settingsPage.Name = "settingsPage";
            this.settingsPage.Padding = new System.Windows.Forms.Padding(3);
            this.settingsPage.Size = new System.Drawing.Size(397, 120);
            this.settingsPage.TabIndex = 1;
            this.settingsPage.Text = "Settings";
            this.settingsPage.UseVisualStyleBackColor = true;
            // 
            // statusBar
            // 
            this.statusBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusBarText});
            this.statusBar.Location = new System.Drawing.Point(0, 164);
            this.statusBar.Name = "statusBar";
            this.statusBar.Size = new System.Drawing.Size(429, 22);
            this.statusBar.SizingGrip = false;
            this.statusBar.TabIndex = 1;
            // 
            // statusBarText
            // 
            this.statusBarText.Name = "statusBarText";
            this.statusBarText.Size = new System.Drawing.Size(43, 17);
            this.statusBarText.Text = "<null>";
            this.statusBarText.Visible = false;
            // 
            // Client
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(429, 186);
            this.Controls.Add(this.statusBar);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Client";
            this.Text = "Bitly Client";
            this.tabControl1.ResumeLayout(false);
            this.controlPage.ResumeLayout(false);
            this.controlPage.PerformLayout();
            this.linkPanel.ResumeLayout(false);
            this.linkPanel.PerformLayout();
            this.statusBar.ResumeLayout(false);
            this.statusBar.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage controlPage;
        private System.Windows.Forms.TabPage settingsPage;
        private System.Windows.Forms.Button shortenButton;
        private System.Windows.Forms.TextBox longUrlLink;
        private System.Windows.Forms.LinkLabel shortUrlLink;
        private System.Windows.Forms.Panel linkPanel;
        private System.Windows.Forms.StatusStrip statusBar;
        private System.Windows.Forms.ToolStripStatusLabel statusBarText;
    }
}

