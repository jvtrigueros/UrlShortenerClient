﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

using BitlyLib;

namespace UrlShortenerClient
{

    public partial class Client : Form
    {
        Bitly bitlyHandle;
        
        // Create event handles for the text change property

        public Client()
        {
            InitializeComponent();
            bitlyHandle = new Bitly("trigoman", "R_92b500c38ea8aa005cfe3202ef2f40f1");
        }

        private void shortenButton_Click(object sender, EventArgs e)
        {
            // Set the wait cursor
            Cursor.Current = Cursors.WaitCursor;

            // Hide the statusbar message
            statusBarText.Visible = false;

            bitlyHandle.shortenUrlRequest(longUrlLink.Text);
            bitlyHandle.PropertyChanged += new PropertyChangedEventHandler(bitlyHandle_PropertyChanged);
        }

        private void bitlyHandle_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            String result = ((Bitly)sender).HttpRequestResult;
            Console.WriteLine(result);
            shortUrlLink.Text = result;
            shortUrlLink.Visible = true;

            // Send the click event to copy it to the clipboard
            shortUrlLink_LinkClicked(sender, null);

            // Set status bar message
            statusBarText.Text = "Short link has been copied to clipboard!";
            statusBarText.Visible = true;

            // Unset the wait cursor
            Cursor.Current = Cursors.Default;
        }

        /// <summary>
        /// This evet will trigger when the Enter key is pressed.
        /// </summary>
        /// <param name="sender">This should be the text field.</param>
        /// <param name="e"></param>
        private void longUrlLink_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            // Only allow the stuff to be sent if the field contains something
            if (e.KeyChar == (char)Keys.Enter && !String.IsNullOrEmpty(longUrlLink.Text) )
            {
                Debug.WriteLine("Is this even being triggered?");
                shortenButton_Click(sender, null);
            }
        }

        private void shortUrlLink_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            // Copy to clipboard
            Clipboard.SetText(bitlyHandle.HttpRequestResult);
        }
    }
}
